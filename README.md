# Transfer.sh

Simple to use file sharing service supporting http upload.
Learn more: https://github.com/dutchcoders/transfer.sh/

## Dependencies
- reverse-proxy

## Setup
```
# Configuration
cp .env.template .env && vi .env

# Create data folders with proper permissions
mkdir -p      ./data/files ./data/temp
chown -R 5000:5000 ./data/
```

## Usage
```
# Upload
curl --upload-file somefile.zip https://transfer.example.com/somefile.zip -u "someuser:somepassword"

#Download
wget https://transfer.example.com/get/randomKey/somefile.zip
```
